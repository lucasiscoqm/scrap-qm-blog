const fetch = require('node-fetch');
var jsdom = require('jsdom').JSDOM;
const fs = require('fs');

const get_sources_with_page = async (page) =>  {
	const res = await fetch("https://www.quantmetry.com/wp-content/themes/Avada-Child-Theme/ajax/query.php", {
		"headers": {
	    "accept": "*/*",
	    "accept-language": "en-US,en;q=0.9,fr-FR;q=0.8,fr;q=0.7,zh-CN;q=0.6,zh;q=0.5",
	    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
	    "x-requested-with": "XMLHttpRequest",
	     "Referer": "https://www.quantmetry.com/blog/",
    	"Referrer-Policy": "strict-origin-when-cross-origin",
		},
	  "body": `q=posts&p=${page}`,
	  "method": "POST"
	});
	return res.text()
}

const get_art_urls = async (html) => {
	const dom = (new jsdom(html)).window.document
	return [...dom.querySelectorAll(".q_news_item_link").values()].map((a)=>a.href)
}



const run  = async (path) => {
	var i = 1;
	let art_urls = []
	while (true) {
		process.stdout.write(`[get_sources] Run: get page ${i}    \r`);
		let html = await get_sources_with_page(i)
		if(html.length ==0) break
		art_urls=[...art_urls,...(await get_art_urls(html))]
		i = i + 1;
	}
	process.stdout.write(`[get_sources] Run: finished    \r`);
	fs.writeFileSync(path,art_urls.join("\n"))
}

(async () => {
	if (process.argv.length <3){
	    console.log("USAGE: node get_sources.js [PATH_TO_THE_SOURCES_TXT]")
	    process.exit(1)
	}
	var path = process.argv.slice(2)[0];
	await run(path);
})()