const puppeteer = require('puppeteer');
const fs = require('fs');

const html_to_pdf = async (page, path, max) => {
    //Get HTML content from HTML file
    const length = fs.readdirSync('pdf/').length
    process.stdout.write(`[transform_html_to_pdf] Run: ${Math.round(100*length/max)}%\r`)
    const html = fs.readFileSync(path, 'utf-8');
    await page.setContent(html, { waitUntil: 'domcontentloaded' });

    // To reflect CSS used for screens instead of print
    await page.emulateMediaType('screen');

    const filename = path.split("/").slice(-1)[0];
    // Downlaod the PDF
    const pdf = await page.pdf({
      path: "pdf/"+filename.replace('.html','.pdf'),
      margin: { top: '100px', right: '50px', bottom: '100px', left: '50px' },
      printBackground: true,
      format: 'A4',
    });
};

(async () => {

  if (process.argv.length <3){
    console.log("USAGE: node transform_html_to_pdf.js [PATH_TO_THE HTML_DIRECTORY]")
    process.exit(1)
  }
  // Get type of source from process.argv, default to url
  var path = process.argv.slice(2)[0]

  // Create a browser instance
  const browser = await puppeteer.launch({headless: "old"});

  // Create a new page
  const page = await browser.newPage();

  const max = fs.readdirSync(path).length
  const dir = await fs.promises.opendir(path)
  for await (const dirent of dir) {
    await html_to_pdf(page, path+"/"+dirent.name, max)
  }
  process.stdout.write(`[transform_html_to_pdf] Run: finished\r`)

  // Close the browser instance
  await browser.close();
  
})();