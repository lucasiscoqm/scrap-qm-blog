# Extract QM blog article in pdf
## One-liner
```sh
make all
# OR
make install && make get_sources && make download_extract_html && make transform_html_to_pdf
# OR
npm install && node get_sources.js sources.txt && node download_extract_html.js sources.txt && node transform_html_to_pdf.js html
```

## Install deps
```sh
make install 
# OR
npm install
```

## Get sources (blog articles urls)
- Execute the script - This will create the file "sources.txt" with all qm blog articles urls
```sh
make get_sources
# OR
node get_sources.js sources.txt
```

## Get html content, extract article part and save to html file in the html directory
- Execute the script
```sh
make download_extract_html
# OR
node download_extract_html.js sources.txt
```

## Convert html to pdf in the pdf directory
- Execute the script
```sh
make transform_html_to_pdf
# OR
node transform_html_to_pdf.js html
```

## Enjoy - The pdf directory contains articles from the qm blog.