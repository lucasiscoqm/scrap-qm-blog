var jsdom = require('jsdom').JSDOM;
const fs = require('fs');

const extractHtmlContentFromUrl = async (url, id) => {
	const dom = await jsdom.fromURL(url);
	const content = dom.window.document.getElementById(id)
	return content.innerHTML
}

const run_for_url = async (url, max) => {
	let length = fs.readdirSync('html/').length
    process.stdout.write(`[transform_html_to_pdf] Run: ${Math.round(100*length/max)}%      \r`)
    let html = await extractHtmlContentFromUrl(url, "article");
	fs.writeFileSync("html/"+url.split("/").slice(-2,-1)[0]+".html", html)
	length = fs.readdirSync('html/').length
	process.stdout.write(`[transform_html_to_pdf] Run: ${Math.round(100*length/max)}%      \r`)
} 
const run_async = async (path) => {
	const content = fs.readFileSync(path).toString('utf8').split('\n')
	await Promise.all(content.map((e)=>run_for_url(e, content.length)))
	process.stdout.write(`[download_extract_html] Run: finished      \r`)
}

const run = async (path) => {
	const content = fs.readFileSync(path).toString('utf8').split('\n')
	for (var i = 0; i < content.length; i++) {
		let url = content[i];
		// process.stdout.write(`[download_extract_html] Run: ${Math.round((i/content.length)*100)}%\r`)
		await run_for_url(url, content.length)
	}	
	process.stdout.write(`[download_extract_html] Run: finished      \r`)
}


(async () => {
	  if (process.argv.length <3){
	    console.log("USAGE: node download_extract_html.js [PATH_TO_THE_SOURCES_TXT]")
	    process.exit(1)
	  }
		var path = process.argv.slice(2)[0];
		await run_async(path);

})()