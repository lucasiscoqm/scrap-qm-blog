.PHONY: usage install get_sources download_extract_html transform_html_to_pdf
.DEFAULT_GOAL := usage
_GREY=\x1b[30m
_RED=\x1b[31m
_GREEN=\x1b[32m
_YELLOW=\x1b[33m
_BLUE=\x1b[34m
_PURPLE=\x1b[35m
_CYAN=\x1b[36m
_WHITE=\x1b[37m
_END=\x1b[0m
_BOLD=\x1b[1m
_UNDER=\x1b[4m
_REV=\x1b[7m

usage:
	@echo "${_CYAN}Makefile${_END}\n${_CYAN}Command${_END}"
	@grep -E '^[a-zA-Z_-]+:$$' "$(CURDIR)/Makefile" | sed -rn "s/(.+)./\1/p"

all: install get_sources download_extract_html transform_html_to_pdf

install:
	npm install

get_sources:
	node get_sources.js sources.txt

download_extract_html:
	mkdir -p html
	node download_extract_html.js sources.txt

transform_html_to_pdf:
	mkdir -p pdf
	node transform_html_to_pdf.js html